#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

#include "DHT.h"

#define DHTPIN 2     // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.
int btpin = 3;
int on = HIGH;
int off = LOW;
int isRun = 0;
int btnState;
// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);

#include <Ultrasonic.h>

Ultrasonic ultrasonic(12,11);

void setup()
{
  // initialize the LCD
  lcd.begin();
  lcd.clear();

  // Turn on the blacklight and print a message.
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Hello, Chamnol!");
  Serial.begin(9600);
  pinMode(btpin,INPUT);
  dht.begin();
}

void loop()
{
  lcd.clear();
  delay(1000);
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);
  lcd.print(F("Temp:"));
  lcd.print(t);
  lcd.print(F(" C "));
  delay(1000);
  lcd.setCursor(0, 1);
  lcd.print(F("Hum:"));
  lcd.print(h);
  delay(2000);
  lcd.clear();
  lcd.print("distance: ");
  lcd.print(ultrasonic.Ranging(CM)); // CM or INC
  lcd.print(" cm" );
  delay(2000);
  
}

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

const char* ssid = "Android007";
const char* password = "abcd12345678";
// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);
// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String output5State = "off";
String output4State = "off";
String isOpening = "false";

#include<Servo.h>                //Adding the Servo library

Servo myservo;
#include <Ultrasonic.h>           //lcd start
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);     //lcd end

Ultrasonic ultrasonic(D7, D8);
Ultrasonic ultrasonicCap(D5, D6);
int distance;
int distance2;           //Define an object named MyServo
const int echo = D8;
const int trig = D7;
int mesafe;
int sure;
int servoPin = D3;

void setup() {
  Serial.begin(115200);
  myservo.attach(servoPin);
  // initialize the LCD
  lcd.begin();
  // Turn on the blacklight and print a message.
  lcd.backlight();
  lcd.print("Welcome!!");
  delay(10);
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
//  pinMode(trig, OUTPUT);
//  pinMode(echo, INPUT);
}

void loop() {
  delay(100);

  //  distance = ultrasonic.read();
  distance2 = ultrasonicCap.Ranging(CM);//lcd start
  Serial.println(distance2);
  float progress = distance2 * 100 / 31;
  //  Serial.println(ultrasonic.distanceRead());
  //  Serial.println(ultrasonicCap.distanceRead());
  if (distance2 <= 10 && distance2 >= 0) {
    lcd.clear();
    lcd.print("I'm full  ");
  } else {
    lcd.clear();
    lcd.print(String(progress) + "%");
  }

  if (output4State == "on") {
    
    mesafe = ultrasonic.Ranging(CM);
    Serial.print(mesafe);

    if (mesafe <= 5) {
      myservo.write(90);
      delay(1000);
      myservo.write(0);
    }
    delay(2000);
    Serial.print("mesafe");
    Serial.println(mesafe);
  }
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  Serial.println("WiFi connected");

  // Wait until the client sends some data
  Serial.println("new client");
  while (!client.available()) {
    delay(1);
  }

  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  String currentLine = "";
  while (client.connected()) {            // loop while the client's connected
    if (client.available()) {
      char c = client.read();             // read a byte, then
      Serial.write(c);                    // print it out the serial monitor
      header += c;
      if (c == '\n') {                    // if the byte is a newline character
        // if the current line is blank, you got two newline characters in a row.
        // that's the end of the client HTTP request, so send a response:
        if (currentLine.length() == 0) {

          Serial.println(progress);

          client.println("HTTP/1.1 200 OK");
          client.println("Content-type:text/html");
          client.println("Connection: close");
          client.println();

          // turns the GPIOs on and off
          if (req.indexOf("GET /5/on") >= 0) {
            Serial.println("GPIO 5 on");
            output5State = "on";
          } else if (req.indexOf("GET /5/off") >= 0) {
            Serial.println("GPIO 5 off");
            output5State = "off";
          } else if (req.indexOf("GET /4/on") >= 0) {
            Serial.println("GPIO 4 on");
            output4State = "on";
          } else if (req.indexOf("GET /4/off") >= 0) {
            Serial.println("GPIO 4 off");
            output4State = "off";
          }
          String s = "<!DOCTYPE html> <html> <head> <title>Trash Me</title> <meta charset=\"utf-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <style> html { height: 100%; margin: 0; } body { background-color: lightblue; display: -webkit-box; display: -ms-flexbox; display: -webkit-flex; display: flex; flex-direction: column; height: 100%; margin: 0; color: darkorange; font-weight: bold; } p { font-family: verdana; font-size: 20px; } #container { width: 40%; height: 75%; background-color: transparent; margin: 35px; flex-direction: column; } .button { background-color: green; height: 50px; width: 200px; border: 2px; border-color: white; border-radius: 25px; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; font-weight: bold; margin: 4px 2px; cursor: pointer; } .switch { position: relative; display: inline-block; width: 60px; height: 34px; } .switch input { opacity: 0; width: 0; height: 0; } .slider { position: absolute; cursor: pointer; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; -webkit-transition: .4s; transition: .4s; } .slider:before { position: absolute; content: \"\"; height: 26px; width: 26px; left: 4px; bottom: 4px; background-color: white; -webkit-transition: .4s; transition: .4s; } input:checked+.slider { background-color: #2196F3; } input:focus+.slider { box-shadow: 0 0 1px #2196F3; } input:checked+.slider:before { -webkit-transform: translateX(26px); -ms-transform: translateX(26px); transform: translateX(26px); } /* Rounded sliders */ .slider.round { border-radius: 34px; } .slider.round:before { border-radius: 50%; } .progressBar { background-color: lightgrey; width: 200px; height: 20px; }";
          s += ".progress { width: " + String(100 - progress) + "%; height: 100%; background-color: blue; animation: color 5s linear 0s alternate; } html += myVariable; html +=keyframes color { 10% { background-color: #0f0; width: 20%; /*At 10%, change the width to 20%*/ } 40% { background-color: lightgreen; width: 40%; } 70% { background-color: lightcoral; width: 60%; } 100% { background-color: darkred; width: 100%; } </style> <script> function submitButtonStyle(_this) { _this.style.backgroundColor = _this.style.backgroundColor === \"green\" ? \"red\" : \"green\"; document.getElementById(\"closeButton\").innerHTML = _this.style.backgroundColor === \"green\" ? \"OPEN\" : \"CLOSE\" } </script> </head> <body background=\"https://checkerscleaningsupply.com/wp-content/uploads/2018/05/Earth-Day-Image.jpg\"> <div id=\"container\"> <h2>Trash Me</h2> <p>The best app to contol smart bin</p>";

          if (output5State == "off") {
            s += "<p><a href=\"/5/on\"><button class=\"button\" id=\"closeButton\" type=\"submit\">OPEN</button></a></p>";
          } else {
            s += "<p><a href=\"/5/off\"><button class=\"button\" id=\"closeButton\" style=\"background-color: gray\"  type=\"submit\">OPENNING</button></a></p>";
          }

          s += "<h2>Enable Sensor</h2><p>State " + output4State + "</p>";
          // If the output4State is off, it displays the ON button
          if (output4State == "off") {
            s += "<p><a href=\"/4/on\"><button class=\"button\">ON</button></a></p>";
          } else {
            s += "<p><a href=\"/4/off\"><button class=\"button\" style=\"background-color: red\">OFF</button></a></p>";
          }
          s += "</div> <div style=\"margin:30px; width: 25%\"> <label><h2>Bin Capacity:</h2></label> <div class=\"progressBar\"> <div class=\"progress\"> </div> </div> </div> </body> </html>";

          client.print(s);
          client.println();
          if (output5State == "off") {
            myservo.write(90);
            isOpening = "true";
          } else {
            myservo.write(0);
            isOpening = "false";
          }
          break;
        } else { // if you got a newline, then clear currentLine
          currentLine = "";
        }
      } else if (c != '\r') {  // if you got anything else but a carriage return character,
        currentLine += c;      // add it to the end of the currentLine
      }
    }
  }
  header = "";
  delay(100);
  Serial.println("Client disonnected");
}

#include<Servo.h>                //Adding the Servo library

#include <Ultrasonic.h>           //lcd start
#define DHTPIN 2     // Digital pin connected to the DHT sensor
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);     //lcd end

Ultrasonic ultrasonic(12, 13);
Ultrasonic ultrasonicCap(9, 10);
int distance;
int distance2;

Servo myservo;            //Define an object named MyServo
const int eko=8;        
const int trig=7;        
int mesafe;
int sure;

void setup() {
  myservo.attach(9);           //We introduced the ninth pin arduino to the servo motor
  pinMode(trig,OUTPUT);        
  pinMode(eko,INPUT);     

  Serial.begin(9600);                       //lcd start
  Serial.println(F("DHTxx test!"));

  // initialize the LCD
  lcd.begin();
  // Turn on the blacklight and print a message.
  lcd.backlight();                          //lcd end
}

void loop() {

  distance = ultrasonic.read();
  distance2 = ultrasonicCap.read();//lcd start
  Serial.println(ultrasonic.distanceRead());
  Serial.println(ultrasonicCap.distanceRead());
//  delay(1000);
  if(distance2<=50) {
    lcd.print("I'm full  ");
      delay(2000);
  }
  if(distance2>50)
  
lcd.print("Distance: ");
lcd.print(distance2);
lcd.print("cm");
  delay(2000);
  lcd.clear();                              //lcd end
  
  myservo.write(0);       
  Serial.begin(9600);     
 
  digitalWrite(trig, LOW);         
  delayMicroseconds(10);
  digitalWrite(trig, HIGH);       
  delayMicroseconds(20);
  digitalWrite(trig, LOW);         
  sure = pulseIn(eko, HIGH);      
  mesafe= (sure/29.1)/2; 

  if(mesafe<=5){
    myservo.write(160);
    delay(5000);   
  }
  if(mesafe>200)
  
mesafe=200;
Serial.print("mesafe");
Serial.println(mesafe);
delay(500);
}

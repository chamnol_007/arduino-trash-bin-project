#include <Ultrasonic.h>
#define DHTPIN 2     // Digital pin connected to the DHT sensor
/////////////////////////////
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

Ultrasonic ultrasonic(12, 13);
int distance;

void setup()
{
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  // initialize the LCD
  lcd.begin();
  // Turn on the blacklight and print a message.
  lcd.backlight();
}

void loop() {
  distance = ultrasonic.read();
  Serial.println(ultrasonic.distanceRead());
//  delay(1000);

  if(distance<=50) {
    lcd.print("I'm full  ");
      delay(2000);
  }
  if(distance>50)
  
lcd.print("Distance: ");
lcd.print(distance);
lcd.print("cm");
  delay(2000);
  lcd.clear();
  }
